package com.example.securtyapi.util;

import com.example.securtyapi.exception.CitiesNotFoundException;
import com.example.securtyapi.exception.FreezeStatusNotFoundException;
import com.example.securtyapi.exception.RegionNotFoundException;
import com.example.securtyapi.exception.RolesNotFoundException;
import com.example.securtyapi.exception.ZoneNotFoundException;

public class ExceptionUtil {

    public static void cashierFkeyException(Exception ex, Long zoneId, Long citiesId, Long regionId,
                                            Long freezeId) {
        if (ex.getMessage().contains("cashier_zone_fkey"))
            throw new ZoneNotFoundException(zoneId);
        if (ex.getMessage().contains("cashier_cities_fkey"))
            throw new CitiesNotFoundException(citiesId);
        if (ex.getMessage().contains("cashier_region_fkey"))
            throw new RegionNotFoundException(regionId);
        if (ex.getMessage().contains("cashier_freeze_fkey"))
            throw new FreezeStatusNotFoundException(freezeId);
    }

    public static void userFkeyException(Exception ex, int roleId) {
        if (ex.getMessage().contains("user_role_fkey"))
            throw new RolesNotFoundException(roleId);
    }
}
