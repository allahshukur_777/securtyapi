package com.example.securtyapi.service.impl;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.repository.RoleRepository;
import com.example.securtyapi.service.RoleService;
import com.example.securtyapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public List<RoleDto> getAll(Pageable pageable) {
        return roleRepository.findAll(pageable);
    }

    @Override
    public RoleSaveDto save(RoleSaveDto roleSave){
        return roleRepository.save(roleSave);
    }

    @Override
    public RoleUpdateDto update(Long id, RoleUpdateDto roleUpdate) {
        return roleRepository.update(id, roleUpdate);
    }

    @Override
    public void deleteById(Long id) {
         roleRepository.deleteById(id);
    }
}
