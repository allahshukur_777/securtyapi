package com.example.securtyapi.service.impl;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.repository.UserRepository;
import com.example.securtyapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public List<UserDto> getAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public UserDto get(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public UserSaveDto save(UserSaveDto userSave){
        return userRepository.save(userSave);
    }

    @Override
    public UserUpdateDto update(Long id, UserUpdateDto userUpdate) {
        return userRepository.update(id, userUpdate);
    }

    @Override
    public void deleteById(Long id) {
         userRepository.deleteById(id);
    }
}
