package com.example.securtyapi.service.impl;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.repository.PermissionRepository;
import com.example.securtyapi.service.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository permissionRepository;

    @Override
    public List<PermissionDto> getAll(Pageable pageable) {
        return permissionRepository.findAll(pageable);
    }

    @Override
    public PermissionSaveDto save(PermissionSaveDto permissionSave){
        return permissionRepository.save(permissionSave);
    }

    @Override
    public PermissionUpdateDto update(Long id, PermissionUpdateDto permissionUpdate) {
        return permissionRepository.update(id, permissionUpdate);
    }

    @Override
    public void deleteById(Long id) {
         permissionRepository.deleteById(id);
    }
}
