package com.example.securtyapi.service;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import jooq.tables.pojos.Role;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RoleService {

    List<RoleDto> getAll(Pageable pageable);

    RoleSaveDto save(RoleSaveDto roleSaveDto);

    RoleUpdateDto update(Long id, RoleUpdateDto roleUpdateDto);

    void deleteById(Long id);
}
