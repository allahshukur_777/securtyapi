package com.example.securtyapi.enums;

import com.example.securtyapi.exception.RolesNotFoundException;
import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.example.securtyapi.enums.PermissionEnum.CASHIER_DELETE;
import static com.example.securtyapi.enums.PermissionEnum.CASHIER_PUT;
import static com.example.securtyapi.enums.PermissionEnum.CASHIER_READ;
import static com.example.securtyapi.enums.PermissionEnum.CASHIER_WRITE;
import static com.example.securtyapi.enums.PermissionEnum.USER_DELETE;
import static com.example.securtyapi.enums.PermissionEnum.USER_PUT;
import static com.example.securtyapi.enums.PermissionEnum.USER_READ;
import static com.example.securtyapi.enums.PermissionEnum.USER_WRITE;

public enum RoleEnum {
    ADMIN(Sets.newHashSet(CASHIER_READ, CASHIER_WRITE, CASHIER_PUT, CASHIER_DELETE, USER_READ, USER_WRITE, USER_PUT, USER_DELETE), 1),
    USER(Sets.newHashSet(CASHIER_READ, CASHIER_WRITE, CASHIER_PUT, USER_READ, USER_WRITE, USER_PUT), 2),
    CASHIER(Sets.newHashSet(CASHIER_READ, USER_READ), 3);

    private final Set<PermissionEnum> permissionEnums;
    private final Integer id;

    RoleEnum(Set<PermissionEnum> permissionEnums, Integer id) {
        this.permissionEnums = permissionEnums;
        this.id = id;
    }

    public Set<PermissionEnum> getPermissionEnums() {
        return permissionEnums;
    }

    public Integer getId() {
        return this.id;
    }

    public static RoleEnum getEnum(Integer value) {
        switch (value) {
            case 1:
                return ADMIN;
            case 2:
                return USER;
            case 3:
                return CASHIER;
            default:
                throw new RolesNotFoundException(value);
        }
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthority(){
       Set<SimpleGrantedAuthority> permissions = getPermissionEnums().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
       permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
       return permissions;
    }
}
