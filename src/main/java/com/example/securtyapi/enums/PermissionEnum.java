package com.example.securtyapi.enums;

public enum PermissionEnum {
    CASHIER_READ("CASHIER_READ"),
    CASHIER_WRITE("CASHIER_WRITE"),
    CASHIER_PUT("CASHIER_PUT"),
    CASHIER_DELETE("CASHIER_DELETE"),
    USER_READ("USER_READ"),
    USER_WRITE("USER_WRITE"),
    USER_PUT("USER_PUT"),
    USER_DELETE("USER_DELETE");

    private final String permission;

    PermissionEnum(String permission){
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
