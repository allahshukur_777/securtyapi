package com.example.securtyapi.auth;

import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username)
                .map(
                        (userDto) -> User.withUsername(username)
                                .disabled(false)
                                .password(userDto.getPassword())
                                .authorities(RoleEnum.getEnum(userDto.getRole()).getGrantedAuthority())
                                .build())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
