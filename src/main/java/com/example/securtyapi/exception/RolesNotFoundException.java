package com.example.securtyapi.exception;

public class RolesNotFoundException extends RuntimeException {

    public RolesNotFoundException(int id) {
        super(id + " Role not found");
    }
}
