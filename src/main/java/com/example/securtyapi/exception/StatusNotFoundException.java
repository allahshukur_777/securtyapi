package com.example.securtyapi.exception;

public class StatusNotFoundException extends RuntimeException {

    public StatusNotFoundException(int id) {
        super(id + " Status not found");
    }
}
