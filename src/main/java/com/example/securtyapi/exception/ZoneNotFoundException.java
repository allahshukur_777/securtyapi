package com.example.securtyapi.exception;

public class ZoneNotFoundException extends RuntimeException {

    public ZoneNotFoundException(Long id) {
        super(id + " Zone not found");
    }
}
