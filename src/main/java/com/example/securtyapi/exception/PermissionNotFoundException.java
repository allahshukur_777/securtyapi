package com.example.securtyapi.exception;

public class PermissionNotFoundException extends RuntimeException {

    public PermissionNotFoundException(int id) {
        super(id + " Permission not found");
    }
}
