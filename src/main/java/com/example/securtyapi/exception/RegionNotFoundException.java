package com.example.securtyapi.exception;

public class RegionNotFoundException extends RuntimeException {

    public RegionNotFoundException(Long id) {
        super(id + " Region not found");
    }
}
