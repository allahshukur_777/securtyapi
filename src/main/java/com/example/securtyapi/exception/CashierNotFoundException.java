package com.example.securtyapi.exception;

public class CashierNotFoundException extends RuntimeException {

    public CashierNotFoundException(Long id) {
        super(id + " Cashier not found");
    }
}
