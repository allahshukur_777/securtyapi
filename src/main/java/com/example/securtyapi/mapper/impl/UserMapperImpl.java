package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.mapper.UserMapper;
import jooq.tables.pojos.User;
import jooq.tables.records.UserRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFullName(user.getFullName());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setRole(RoleEnum.getEnum(user.getRole()).getId());
        return userDto;
    }

    @Override
    public List<UserDto> toUserDto(Result<Record> recordResult) {
//        Map<Long, CashierDto> cashierMap = new HashMap<>();
//
//        recordResult.forEach(r -> {
//
//            CashierDto cashier = cashierMap.get(r.getValue(CASHIER.ID));
//            if (cashier == null) {
//                cashier = new CashierDto();
//                cashier.setId(r.getValue(CASHIER.ID));
//                cashier.setCashierCode(r.getValue(CASHIER.CASHIER_CODE));
//                cashier.setProviderId(r.getValue(CASHIER.PROVIDER_ID));
//                cashier.setFullName(r.getValue(CASHIER.FULL_NAME));
//                cashier.setMobile(r.getValue(CASHIER.MOBILE));
//                cashier.setPhone(r.getValue(CASHIER.PHONE));
//                cashier.setEmail(r.getValue(CASHIER.EMAIL));
//                cashier.setSalesRepresentative(r.getValue(CASHIER.SALES_REPRESENTATIVE));
//                cashier.setZone(r.getValue(CASHIER.ZONE));
//                cashier.setCities(r.getValue(CASHIER.CITIES));
//                cashier.setRegion(r.getValue(CASHIER.REGION));
//                cashier.setAddress(r.getValue(CASHIER.ADDRESS));
//                cashier.setMacAddress(r.getValue(CASHIER.MAC_ADDRESS));
//                cashier.setNextPermanentBalance(r.getValue(CASHIER.NEXT_PERMANENT_BALANCE));
//                cashier.setCurrentBalance(r.getValue(CASHIER.CURRENT_BALANCE));
//                cashier.setDebtCredit(r.getValue(CASHIER.DEBT_CREDIT));
//                cashier.setExtraDebtCredit(r.getValue(CASHIER.EXTRA_DEBT_CREDIT));
//                cashier.setMinStake(r.getValue(CASHIER.MIN_STAKE));
//                cashier.setMaxStake(r.getValue(CASHIER.MAX_STAKE));
//                cashier.setBetTicketPayoutLimit(r.getValue(CASHIER.BET_TICKET_PAYOUT_LIMIT));
//                cashier.setVoucherPayoutLimit(r.getValue(CASHIER.VOUCHER_PAYOUT_LIMIT));
//                cashier.setStatus(StatusEnum.getEnum(r.getValue(CASHIER.STATUS)).name());
//                cashier.setFreezeStatus(r.getValue(CASHIER.FREEZE_STATUS));
//                cashier.setUserName(r.getValue(CASHIER.USER_NAME));
//                cashier.setPassword(r.getValue(CASHIER.PASSWORD));
//                cashier.setLastLogin(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGIN)));
//                cashier.setLastLogout(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGOUT)));
//                cashier.setCreationDate(DateUtil.getLong(r.getValue(CASHIER.CREATION_DATE)));
//                cashier.setRole(RoleEnum.getEnum(r.getValue(CASHIER.ROLE)).getId());
//                cashierMap.put(cashier.getId(), cashier);
//            }
//        });
//
////        List<String> permissionList = new ArrayList<>();
////
////        addPermissionMap(cashierDto.getPermission(), cashierDto, permissionList);
////
////
//        return new ArrayList<>(cashierMap.values());
        return null;
    }

    @Override
    public UserSaveDto toUserSaveDto(UserRecord userRecord) {
        return new UserSaveDto(
                userRecord.getFullName(),
                userRecord.getUsername(),
                userRecord.getPassword(),
                RoleEnum.getEnum(userRecord.getRole()).getId());
    }

    @Override
    public UserUpdateDto toUserUpdateDto(UserRecord userRecord) {
        return new UserUpdateDto(
                userRecord.getId(),
                userRecord.getFullName(),
                userRecord.getUsername(),
                userRecord.getPassword(),
                RoleEnum.getEnum(userRecord.getRole()).getId());
    }

    private void addPermissionMap(Map<Integer, List<String>> permissionMap, CashierDto cashierDto, List<String> permissionList) {
        if (permissionMap == null) {
            permissionMap = new HashMap<>();
        }
    }
}