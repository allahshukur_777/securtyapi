package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.enums.StatusEnum;
import com.example.securtyapi.mapper.PermissionMapper;
import com.example.securtyapi.mapper.RoleMapper;
import jooq.tables.pojos.Permission;
import jooq.tables.pojos.Role;
import jooq.tables.records.PermissionRecord;
import jooq.tables.records.RoleRecord;

public class PermissionMapperImpl implements PermissionMapper {

    @Override
    public PermissionDto toPermissionDto(Permission permission) {
        PermissionDto permissionDto = new PermissionDto();
        permissionDto.setId(permission.getId());
        permissionDto.setName(permission.getName());
        return permissionDto;
    }

    @Override
    public PermissionSaveDto toPermissionSaveDto(PermissionRecord permissionRecord) {
        return new PermissionSaveDto(
                permissionRecord.getName());
    }

    @Override
    public PermissionUpdateDto toPermissionUpdateDto(PermissionRecord permissionRecord) {
        return new PermissionUpdateDto(
                permissionRecord.getId(),
                permissionRecord.getName());
    }
}