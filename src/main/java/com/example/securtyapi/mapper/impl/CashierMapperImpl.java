package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.enums.StatusEnum;
import com.example.securtyapi.mapper.CashierMapper;
import com.example.securtyapi.util.DateUtil;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jooq.Tables.CASHIER;

public class CashierMapperImpl implements CashierMapper {

    @Override
    public CashierDto toCashierDto(Cashier cashier) {
        CashierDto cashierDto = new CashierDto();
        cashierDto.setId(cashier.getId());
        cashierDto.setCashierCode(cashier.getCashierCode());
        cashierDto.setProviderId(cashier.getProviderId());
        cashierDto.setFullName(cashier.getFullName());
        cashierDto.setMobile(cashier.getMobile());
        cashierDto.setPhone(cashier.getPhone());
        cashierDto.setEmail(cashier.getEmail());
        cashierDto.setSalesRepresentative(cashier.getSalesRepresentative());
        cashierDto.setZone(cashier.getZone());
        cashierDto.setCities(cashier.getCities());
        cashierDto.setRegion(cashier.getRegion());
        cashierDto.setAddress(cashier.getAddress());
        cashierDto.setMacAddress(cashier.getMacAddress());
        cashierDto.setNextPermanentBalance(cashier.getNextPermanentBalance());
        cashierDto.setCurrentBalance(cashier.getCurrentBalance());
        cashierDto.setDebtCredit(cashier.getDebtCredit());
        cashierDto.setExtraDebtCredit(cashier.getExtraDebtCredit());
        cashierDto.setMinStake(cashier.getMinStake());
        cashierDto.setMaxStake(cashier.getMaxStake());
        cashierDto.setBetTicketPayoutLimit(cashier.getBetTicketPayoutLimit());
        cashierDto.setVoucherPayoutLimit(cashier.getVoucherPayoutLimit());
        cashierDto.setStatus(StatusEnum.getEnum(cashier.getStatus()).name());
        cashierDto.setFreezeStatus(cashier.getFreezeStatus());
        cashierDto.setUserName(cashier.getUserName());
        cashierDto.setPassword(cashier.getPassword());
        cashierDto.setLastLogin(DateUtil.getLong(cashier.getLastLogin()));
        cashierDto.setLastLogout(DateUtil.getLong(cashier.getLastLogout()));
        cashierDto.setCreationDate(DateUtil.getLong(cashier.getCreationDate()));
        cashierDto.setRole(RoleEnum.getEnum(cashier.getRole()).getId());
        return cashierDto;
    }

    @Override
    public List<CashierDto> toCashierDto(Result<Record> recordResult) {
        Map<Long, CashierDto> cashierMap = new HashMap<>();

        recordResult.forEach(r -> {

            CashierDto cashier = cashierMap.get(r.getValue(CASHIER.ID));
            if (cashier == null) {
                cashier = new CashierDto();
                cashier.setId(r.getValue(CASHIER.ID));
                cashier.setCashierCode(r.getValue(CASHIER.CASHIER_CODE));
                cashier.setProviderId(r.getValue(CASHIER.PROVIDER_ID));
                cashier.setFullName(r.getValue(CASHIER.FULL_NAME));
                cashier.setMobile(r.getValue(CASHIER.MOBILE));
                cashier.setPhone(r.getValue(CASHIER.PHONE));
                cashier.setEmail(r.getValue(CASHIER.EMAIL));
                cashier.setSalesRepresentative(r.getValue(CASHIER.SALES_REPRESENTATIVE));
                cashier.setZone(r.getValue(CASHIER.ZONE));
                cashier.setCities(r.getValue(CASHIER.CITIES));
                cashier.setRegion(r.getValue(CASHIER.REGION));
                cashier.setAddress(r.getValue(CASHIER.ADDRESS));
                cashier.setMacAddress(r.getValue(CASHIER.MAC_ADDRESS));
                cashier.setNextPermanentBalance(r.getValue(CASHIER.NEXT_PERMANENT_BALANCE));
                cashier.setCurrentBalance(r.getValue(CASHIER.CURRENT_BALANCE));
                cashier.setDebtCredit(r.getValue(CASHIER.DEBT_CREDIT));
                cashier.setExtraDebtCredit(r.getValue(CASHIER.EXTRA_DEBT_CREDIT));
                cashier.setMinStake(r.getValue(CASHIER.MIN_STAKE));
                cashier.setMaxStake(r.getValue(CASHIER.MAX_STAKE));
                cashier.setBetTicketPayoutLimit(r.getValue(CASHIER.BET_TICKET_PAYOUT_LIMIT));
                cashier.setVoucherPayoutLimit(r.getValue(CASHIER.VOUCHER_PAYOUT_LIMIT));
                cashier.setStatus(StatusEnum.getEnum(r.getValue(CASHIER.STATUS)).name());
                cashier.setFreezeStatus(r.getValue(CASHIER.FREEZE_STATUS));
                cashier.setUserName(r.getValue(CASHIER.USER_NAME));
                cashier.setPassword(r.getValue(CASHIER.PASSWORD));
                cashier.setLastLogin(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGIN)));
                cashier.setLastLogout(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGOUT)));
                cashier.setCreationDate(DateUtil.getLong(r.getValue(CASHIER.CREATION_DATE)));
                cashier.setRole(RoleEnum.getEnum(r.getValue(CASHIER.ROLE)).getId());
                cashierMap.put(cashier.getId(), cashier);
            }
        });

//        List<String> permissionList = new ArrayList<>();
//
//        addPermissionMap(cashierDto.getPermission(), cashierDto, permissionList);
//
//
        return new ArrayList<>(cashierMap.values());
    }

    @Override
    public CashierSaveDto toCashierSaveDto(CashierRecord cashierRecord) {
        return new CashierSaveDto(
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                RoleEnum.getEnum(cashierRecord.getRole()).getId());
    }

    @Override
    public CashierUpdateDto toCashierUpdateDto(CashierRecord cashierRecord) {
        return new CashierUpdateDto(
                cashierRecord.getId(),
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                RoleEnum.getEnum(cashierRecord.getRole()).getId());
    }

    private void addPermissionMap(Map<Integer, List<String>> permissionMap, CashierDto cashierDto, List<String> permissionList) {
        if (permissionMap == null) {
            permissionMap = new HashMap<>();
        }
    }
}