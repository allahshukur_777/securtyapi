package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.enums.StatusEnum;
import com.example.securtyapi.mapper.RoleMapper;
import com.example.securtyapi.mapper.UserMapper;
import jooq.tables.pojos.Role;
import jooq.tables.pojos.User;
import jooq.tables.records.RoleRecord;
import jooq.tables.records.UserRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoleMapperImpl implements RoleMapper {

    @Override
    public RoleDto toRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }

    @Override
    public RoleSaveDto toRoleSaveDto(RoleRecord roleRecord) {
        return new RoleSaveDto(
                roleRecord.getName());
    }

    @Override
    public RoleUpdateDto toRoleUpdateDto(RoleRecord roleRecord) {
        return new RoleUpdateDto(
                roleRecord.getId(),
                roleRecord.getName());
    }
}