package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import jooq.tables.pojos.User;
import jooq.tables.records.UserRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.List;

public interface UserMapper {

    UserDto toUserDto(User user);

    List<UserDto> toUserDto(Result<Record> recordResult);

    UserSaveDto toUserSaveDto(UserRecord userRecord);

    UserUpdateDto toUserUpdateDto(UserRecord userRecord);
}
