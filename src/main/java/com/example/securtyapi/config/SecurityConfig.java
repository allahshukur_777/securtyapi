package com.example.securtyapi.config;

import com.example.securtyapi.auth.AuthUserDetailsService;
import com.example.securtyapi.enums.PermissionEnum;
import com.example.securtyapi.enums.RoleEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.example.securtyapi.enums.PermissionEnum.CASHIER_DELETE;
import static com.example.securtyapi.enums.PermissionEnum.CASHIER_PUT;
import static com.example.securtyapi.enums.PermissionEnum.CASHIER_WRITE;
import static com.example.securtyapi.enums.RoleEnum.ADMIN;
import static com.example.securtyapi.enums.RoleEnum.CASHIER;
import static com.example.securtyapi.enums.RoleEnum.USER;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthUserDetailsService authUserDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(authUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/cashier");
    }
}
