package com.example.securtyapi.config;

import com.example.securtyapi.mapper.impl.CashierMapperImpl;
import com.example.securtyapi.mapper.impl.PermissionMapperImpl;
import com.example.securtyapi.mapper.impl.RoleMapperImpl;
import com.example.securtyapi.mapper.impl.UserMapperImpl;
import com.example.securtyapi.swagger.SwaggerPageable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@EnableSpringDataWebSupport
public class AppConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(regex("/.*"))
                .build()
                .directModelSubstitute(Pageable.class, SwaggerPageable.class);
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public CashierMapperImpl getCashierMapper() {
        return new CashierMapperImpl();
    }

    @Bean
    public UserMapperImpl getUserMapper() {
        return new UserMapperImpl();
    }

    @Bean
    public RoleMapperImpl getRoleMapper() {
        return new RoleMapperImpl();
    }

    @Bean
    public PermissionMapperImpl getPermissionMapper() {
        return new PermissionMapperImpl();
    }

    @Override // This Override I have used for @PageableDefault
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
    }
}
