package com.example.securtyapi.dto.dto;

import lombok.Data;

@Data
public class FreezeStatusDto {
    private Long id;
    private String name;
}
