package com.example.securtyapi.dto.dto;

import lombok.Data;

@Data
public class CitiesDto {
    private Long id;
    private String name;
}
