package com.example.securtyapi.controller;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.service.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/permission")
public class PermissionController {

    private final PermissionService permissionService;

    @GetMapping
//    @PreAuthorize("hasAuthority('PERMISSION_READ')")
    public List<PermissionDto> getAll(@PageableDefault(size = 5) Pageable pageable) {
        return permissionService.getAll(pageable);
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('PERMISSION_WRITE')")
    public PermissionSaveDto save(@Valid @RequestBody PermissionSaveDto permissionSave) {
        return permissionService.save(permissionSave);
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasAuthority('PERMISSION_PUT')")
    public PermissionUpdateDto update(@PathVariable Long id, @Valid @RequestBody PermissionUpdateDto permissionUpdate) {
        return permissionService.update(id, permissionUpdate);
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('PERMISSION_DELETE')")
    public void deleteById(@PathVariable Long id) {
        permissionService.deleteById(id);
    }
}
