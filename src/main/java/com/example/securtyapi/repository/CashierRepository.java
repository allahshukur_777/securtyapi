package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CashierRepository {

    List<CashierDto> findAll(Pageable pageable);

    CashierSaveDto save(CashierSaveDto cashierSaveDto);

    CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdateDto);

    void deleteById(Long id);

    CashierDto findById(Long id);

    Optional<CashierDto> findCashierByUsername(String username);
}
