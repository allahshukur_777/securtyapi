package com.example.securtyapi.repository.impl;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.exception.RolesNotFoundException;
import com.example.securtyapi.mapper.RoleMapper;
import com.example.securtyapi.repository.RoleRepository;
import jooq.tables.pojos.Role;
import jooq.tables.records.RoleRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static jooq.tables.Role.ROLE;

@Repository
@RequiredArgsConstructor
public class RoleRepositoryImpl implements RoleRepository {

    private final DSLContext dslContext;
    private final RoleMapper roleMapper;

    @Override
    public List<RoleDto> findAll(Pageable pageable) {
        return dslContext.selectFrom(ROLE)
                .where(ROLE.STATUS.equal(false))
                .orderBy(ROLE.ID)
                .fetchInto(Role.class)
                .stream()
                .map(roleMapper::toRoleDto)
                .collect(Collectors.toList());
    }

    @Override
    public RoleSaveDto save(RoleSaveDto roleSave) {
        RoleRecord roleRecord = dslContext.insertInto(ROLE)
                .set(ROLE.NAME, roleSave.getName())
                .returning()
                .fetchOne();
        return roleMapper.toRoleSaveDto(roleRecord);
    }

    @Override
    public RoleUpdateDto update(Long id, RoleUpdateDto roleUpdate) {
        RoleRecord roleRecord = dslContext.update(ROLE)
                .set(ROLE.NAME, roleUpdate.getName())
                .where(ROLE.ID.equal(id))
                .returning()
                .fetchOne();
        if (roleRecord == null) throw new RolesNotFoundException(Math.toIntExact(id));
        return roleMapper.toRoleUpdateDto(roleRecord);
    }

    @Override
    public void deleteById(Long id) {
        int roleSize = dslContext.update(ROLE)
                .set(ROLE.STATUS, true)
                .where(ROLE.STATUS.equal(false)
                        .and(ROLE.ID.equal(id)))
                .execute();
        if (roleSize == 0) throw new RolesNotFoundException(Math.toIntExact(id));
    }
}
